<?php 
if (is_single()) {  //show single post
  require_once('components/single-post.inc.php');
}elseif (is_page()) { ?>
    <p> <?php the_content();?></p>
<?php 
}else {
?>
<div class="col-md-4 mb-4 slideInUp slower wow" data-wow-delay="0.3s" "="" style="visibility: visible; animation-name: slideInUp; animation-delay: 0.3s;">
  <div class="card warning-color-dark ">
    <div class="view">
      <?php the_post_thumbnail( 'medium-large', array( 'class'=> 'card-img-top')); ?>
      <a href="<?php the_permalink();?>" target="_blank">
        <div class="mask rgba-white-slight waves-effect waves-light"></div>
      </a>
    </div>
    <div class="card-body text-center">
      <h6 class="card-title white-text"><?php the_title(); ?></h6>
      <a href="<?php the_permalink();?>" target="_blank" class="btn btn-outline-white btn-md waves-effect">Read More</a>
    </div>
  </div>
</div>

<?php
}
?>