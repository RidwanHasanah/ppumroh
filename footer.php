<!-- SosMed -->
<?php
$options = get_option('sosmed');
$footer = get_option('panel');
?>
<section class="sosmed" id="sosmed">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php if ($options['cfb'] == 1) { ?>
          <a href="<?php echo $options['fb']; ?>" target="_blank" class="black-hover"><i class="fab  fa-facebook-f h3-responsive text-white"></i></a>
        <?php } ?>

        <?php if ($options['ctwitter'] == 1) { ?>
          <a href="<?php echo $options['twitter']; ?>" target="_blank" class="black-hover"><i class="fab  fa-twitter h3-responsive text-white"></i></a>
        <?php } ?>

        <?php if ($options['cgoogle'] == 1) { ?>
          <a href="<?php echo $options['google']; ?>" target="_blank" class="black-hover"><i class="fab  fa-google-plus-g h3-responsive text-white"></i></a>
        <?php } ?>

        <?php if ($options['cinsta'] == 1) { ?>
          <a href="<?php echo $options['insta']; ?>" target="_blank" class="black-hover"><i class="fab  fa-instagram h3-responsive text-white"></i></a>
        <?php } ?>

      </div>
    </div>
  </div>
</section>
<!-- Akhir Sosmed -->
<!-- Wa -->
<a href="https://api.whatsapp.com/send?phone=<?php echo $footer['wa']; ?>&text=" class="float" target="_blank">
  <img class="my-float" src="<?php echo get_template_directory_uri() . '/img/whatsappbtn.png'; ?>" alt="">
</a>
<!-- Akhir Wa -->
<footer class="page-footer">
  <div class="container d-none">
    <div class="row">
      <div class="col-sm-6">
        <?php dynamic_sidebar('footer1'); ?>
      </div>
      <div class="col-sm-6">
        <?php dynamic_sidebar('footer2'); ?>
      </div>
    </div>
  </div>
  <section class="cp" id="cp">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6 class=" text-white text-center">&copy <?php echo $footer['btm']; ?> </h6>
        </div>
      </div>
    </div>
  </section>
</footer>
<script type="text/javascript">
  new WOW().init();
</script>
</body>

</html>