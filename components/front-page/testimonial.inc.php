 <!-- Testimonial  -->
 <?php
 $options = get_option('panel');
 function testimonial_display(){
   ?>
   <div class="col-md-3 zoom p-3 ">
   <?php the_post_thumbnail('medium', array('class' => 'card-img-top')); ?>
      <h5 class="pt-3" ><?php the_title();?></h5>
      <small>
      <?php the_content(); ?>
      </small>
    </div>
   <?php
 }
 ?>
 <section class="p-0" id="testimonial">
    <div class="container text-center pb-5">
      <div class="row">
        <div class="col-sm-12 text-center">
            <h1 class="h1-responsive wow fadeInUp slow mt-5"><?php echo $options['testimonial']; ?></h1>
            <hr class="wow fadeInUp slow">
        </div>
      </div>
      <div class="row pb-5">
      <?php
        query_posts(array('post_type'=> 'testimonial','posts_per_page'=>4));
        if( have_posts() ){
          while(have_posts()){
            the_post();
            testimonial_display();
          }
        }else{
          echo '<h1>Nothing Data</h1>';
        }
        wp_reset_query();
        ?>
      </div>
    </div>
  </section>