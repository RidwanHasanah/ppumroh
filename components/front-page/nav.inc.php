<?php 
$custom_logo_id = get_theme_mod( 'custom_logo' );
$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );

$options = get_option('sosmed');
?>
<header class="fixed-top" id="header">
    <!--Main Navigation-->
    <nav class=" py-0 navbar navbar-expand-lg bgc1 scrolling-navbar satu"> 
      <div class="">
        <ul class="navbar-nav mr-auto nav-flex-icons px-3">
        <?php if($options['cfb'] == 1){ ?>
          <li class="nav-item">
            <a class="nav-link waves-effect waves-light white-hover" target="_blank" href="<?php echo $options['fb']; ?>">
              <i class="fab fa-facebook-f"></i>
            </a>
          </li>
        <?php } ?>

        <?php if($options['ctwitter'] == 1){ ?>
          <li class="nav-item">
            <a class="nav-link waves-effect waves-light white-hover" target="_blank" href="<?php echo $options['twitter']; ?>">
              <i class="fab fa-twitter"></i>
            </a>
          </li>
        <?php } ?>
        
        <?php if($options['cgoogle'] == 1){ ?>
          <li class="nav-item">
            <a class="nav-link waves-effect waves-light white-hover" target="_blank" href="<?php echo $options['google']; ?>">
              <i class="fab fa-google-plus-g"></i>
            </a>
          </li>
        <?php } ?>
        
        <?php if($options['cinsta'] == 1){ ?>
          <li class="nav-item">
            <a class="nav-link waves-effect waves-light white-hover" target="_blank" href="<?php echo $options['insta']; ?>">
              <i class="fab fa-instagram"></i>
            </a>
          </li>
        <?php } ?>
        
        </ul>
        <!-- Search Form -->
        <!-- <form class="form-inline">
              <i class="fas fa-search" aria-hidden="true"></i>
              <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search" aria-label="Search">
            </form> -->
      </div>
      </div>
    </nav>
    <!--Main Navigation-->

    <!--Main Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light scrolling-navbar dua">
      <div class="container-fluid pl-0 ml-0">
      
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
                    $args = array(
                      'menu'            => '',
                      'container'       => 'div',
                      'container_class' => '',
                      'container_id'    => '',
                      'menu_class'      => 'menu px-0 mx-0',
                      'menu_id'         => '',
                      'echo'            => true,
                      'fallback_cb'     => 'wp_page_menu',
                      'before'          => '',
                      'after'           => '',
                      'link_before'     => '',
                      'link_after'      => '',
                      'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                      'item_spacing'    => 'preserve',
                      'depth'           => 0,
                      'walker'          => '',
                      'theme_location'  => 'main_menu',
              );
                    wp_nav_menu($args); 
        ?>
        </div>
          <a class="navbar-brand" href="<?php echo home_url(); ?>">
            <img src="<?php echo $image[0];?>" class="wow fadeInLeft slow" height="50" alt=""/>
          </a>
      </div>
    </nav>
    <!--Main Navigation-->
  </header>