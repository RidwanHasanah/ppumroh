 <!-- Profil Keunggulan-->
 <?php
  $options = get_option('panel');
  function excellence_display1()
  {
    ?>
   <div class="col-md-4 boxzoom text-center wow rotateInUpLeft slower">
     <?php the_post_thumbnail('rounded', array('class' => 'img-fluid rounded-circle z-depth-1 imcon')); ?>
     <h5 class=" pt-3"> <b><a class="text-dark" href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></b></h5>
     <p>
       <?php the_content(); ?>
     </p>
   </div>
 <?php
  }
  function excellence_display2()
  {
    ?>
   <div class="col-md-6 boxzoom text-center bgc2 wow rotateInUpLeft slower">
     <?php the_post_thumbnail('rounded', array('class' => 'img-fluid rounded-circle z-depth-1 imcon')); ?>
     <h5 class=" pt-3"> <b><a class="text-dark" href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></b></h5>
     <p>
       <?php the_content(); ?>
     </p>
   </div>
 <?php
  }
  ?>
 <section class=" " id="excellence">
   <div class="container-fluid pt-1">
     <div class="row bgc1 text-center">
       <div class="col-md-12">
        <h1 class="wow fadeInUp slow text-center mt-5"><?php echo $options['excellence']; ?></h1>
        <hr class="line-white">
      </div>
     </div>

     <div class="row">
       <div class="col-md-6  p-5">
         <div class="row">
           <?php
            query_posts(array('category_name' => 'Keunggulan 1','posts_per_page'=>3));

            if (have_posts()) {
              while (have_posts()) {
                the_post();
                excellence_display1();
              }
            } else {
              echo '<h1>Nothing Data</h1>';
            }
            wp_reset_query();

            ?>
         </div>
       </div>
       <div class="col-md-6 bgc2 p-5">
         <div class="row">
           <?php
             query_posts(array('category_name' => 'Keunggulan 2','posts_per_page'=>2));

            if (have_posts()) {
              while (have_posts()) {
                the_post();
                excellence_display2();
              }
            } else {
              echo '<h1>Nothing Data</h1>';
            }
            wp_reset_query();

            ?>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- Akhir Profil -->