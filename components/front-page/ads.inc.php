<?php $options = get_option('panel'); ?>
<section class="separate bgc1 mb-3" id="separate">
    <div class="container py-5 text-center">
      <h5 class="textshadow py-2 zoom">
        <b>
        <?php echo $options['ads1']; ?>
        </b>
      </h5>
      <h6 class="textshadow py-2 zoom">
        <b>
      <?php echo $options['ads2']; ?>
      </b>
      </h6>
    </div>
  </section>