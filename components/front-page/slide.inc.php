<?php 
$options = get_option('slide');  

$optionslides = get_option('amount_slide');
if ($optionslides['slider'] < 1){ $slide = 1; }
else { $slide = $optionslides['slider']; }

?>

  <div id="sliderCarousel" class="carousel slide vert mtop" data-ride="carousel">
        <div class="carousel-inner bgctranparant">
          <?php
          for ($i=1; $i <= $slide; $i++) { 
            ?>
              <div class="carousel-item <?php if($i==1){ echo 'active'; } ?>">

              <div class="view">
                <img class="d-block w-100 img-fluid" src="<?php echo $options["simg$i"];?>" alt="First slide">
              </div>
              <div class="carousel-caption d-none">
                  <h3 class="h3-responsive wow fadeInUp slow"><?php echo $options["stitle$i"];?></h3>
                  <p class="wow fadeInUp slow"><?php echo $options["sdesc$i"];?></p>
                  <a target="_blank" href="<?php echo $options["sbtn$i"];?>" class="btn btn-sm btn-warning wow fadeInUp slow">Detail</a>
              </div>

              </div>
            <?php
          }
            
           ?>
        <a class="carousel-control-prev" href="#sliderCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#sliderCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
    </div>
  <!--/.Carousel Wrapper-->
  <!-- Akhir SlideShow -->