 <!-- Video -->
 <?php 
 function display_content(){
   ?>
   <div class="col-md-6 mb-4 slideInUp slower wow" data-wow-delay="0.3s" "="" style=" visibility: visible; animation-name: slideInUp; animation-delay: 0.3s;">
      <div class="card bgc1 ">
        <div class="view ">
          <?php the_post_thumbnail('medium', array('class' => 'card-img-top')); ?>
          <a href="<?php the_permalink(); ?>" target="_blank ">
            <div class="mask rgba-white-slight  waves-effect waves-light"></div>
          </a>
        </div>
        <div class="card-body text-center ">
          <a href="<?php the_permalink(); ?>" target="_blank">
            <h5 class="card-title white-text "><b><?php the_title(); ?></b></h5>
          </a>
          <div class="text-white">
            <?php //echo get_the_excerpt(); 
            ?>
          </div>
        </div>
      </div>

    </div>
   <?php
 }
 ?>
 <section class="video" id="video">
   <div class="container-fluid">
     <div class="row bgc1">
       <div class="col-sm-12 text-center">
         <h1 class="h1-responsive wow fadeInUp slow mt-5">GALLERY</h1>
         <hr class="line-white">
       </div>
     </div>
     <div class="row mt-5">
       <div class="col-md-6 p-5">
         <h3 class="text-center h3-responsive"> <img width="50" height="50" src="<?php echo get_template_directory_uri() . '/img/icon/news.png'; ?>" alt=""> BERITA</h3>
         <hr>
         <div class="row">
           <?php
            wp_reset_query();
            query_posts(array('category_name' => 'berita','posts_per_page'=>4));
            if (have_posts()) {
              while (have_posts()) {
                the_post();
                display_content();
              }
            }

            ?>
         </div>
       </div>

       <div class="col-md-6 p-5">
         <h3 class="text-center h3-responsive"> <img width="50" height="50" src="<?php echo get_template_directory_uri() . '/img/icon/artikel.png'; ?>" alt=""> ARTIKEL</h3>
         <hr>
         <div class="row">
           <?php
            wp_reset_query();
            query_posts(array('category_name' => 'artikel','posts_per_page'=>4));
            if (have_posts()) {
              while (have_posts()) {
                the_post();
                display_content();
              }
            }

            ?>
         </div>
       </div>

     </div>
     
   </div>
 </section>