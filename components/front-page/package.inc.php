  <!-- Paket Umrah -->
  <?php
  $options = get_option('panel');
  function package_display(){
    ?>
      <div class="col-lg-6 col-md-6 mb-4">
            <div class="card mb-4">
              <div class="view view-cascade">
                <?php the_post_thumbnail('medium', array('class'=>'card-img-top shadow') ); ?>
                <a target="_blank" class="" href="<?php the_permalink();?>">
                  <div class="mask rgba-white-slight waves-effect waves-light"></div>
                </a>
              </div>
              <div class="card-body">
                <h6 class="card-title text-center"><strong> <a target="_blank" class="text-dark" href="<?php the_permalink();?>"> <?php the_title();?> </a> </strong></h6>
                <p class="card-text">
                <?php //echo get_the_excerpt();?>
                </p>
                <a href="http://azfaharomain.com/aamobile/#/page_iframe/form-pendaftaran-umroh-app" target="_blank" class="btn bgc1 btn-sm text-dark text-center btn-block">Daftar</a>
              </div>
            </div>
          </div>
    <?php
  }
  ?>
  <section class="paketumrah mt-5" id="paketumrah">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 text-center" data-aos="fade-up">
          <h1 class="h1-responsive wow fadeInUp slow"><?php echo $options['package']; ?></h1>
          <hr class="wow fadeInUp slow">
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6">
        <h3 class="text-center h3-responsive">PAKET REGULAR</h3>
        <hr>
        <div class="row px-5">
        <?php
        query_posts(array('category_name' => 'paket regular','posts_per_page'=>4));
        if( have_posts() ){
          while(have_posts()){
            the_post();
            package_display();
          }
        }else{
        echo '<h1>Nothing Data</h1>';
        }
        wp_reset_query(); 
        
        ?>
        </div>
      </div>
      <div class="col-lg-6">
      <h3 class="text-center h3-responsive">PAKET KHUSUS</h3>
        <hr>
      <div class="row px-5">
        <?php
          query_posts(array('category_name' => 'paket khusus','posts_per_page'=>4));
          if( have_posts() ){
            while(have_posts()){
              the_post();
              package_display();
            }
          }else{
          echo '<h1>Nothing Data</h1>';
          }
          wp_reset_query(); 
          
          ?>
          </div>
      </div>
      </div>
    </div>
  </section>
  <!-- Akhir Paket Umrah -->