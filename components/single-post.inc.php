<div class="col-md-6 offset-md-3">
    <?php the_post_thumbnail('big_thumb',array( 'class'=> 'img-post'));?>
    <h1 class="pt-3 text-center"><?php the_title(); ?></h1>
</div>
<div class="col-md-6 offset-md-3" >
<?php the_content(); ?>
</div>

<div>
    <p class="meta_info">
    by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'));?>" target="_blank"><?php the_author() ?></a>
    Posted: <?php the_date('F j, Y'); ?> at <?php the_time('g:i a'); 
    echo ' - Kategori : '; the_category(', '); ?>
    </p>
</div>