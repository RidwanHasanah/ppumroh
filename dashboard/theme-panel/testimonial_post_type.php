<?php
function testimonial_post_type(){
    $supports = array(
        'title', // post title
        'editor', // post content
        'author', // post author
        'thumbnail', // featured images
    );

    $labels = array(
        'name' => _x('testimonials','plural'),
        'singular_name' => _x('testimonial','singular'),
        'menu_name' => _x('Testimonial', 'admin menu'),
        'name_admin_bar' => _x('Testimonial','admin bar'),
        'add_new' => _x('Add New', 'add new'),
        'add_new_item' => __('Add New Testimonial'),
        'new_item' => __('New Testimonial'),
        'edit_item' => __('Edit Testimonial'),
        'view_item' => __('View Testimonial'),
        'all_item' => __('All Testimonial'),
        'search_item' => __('Seacrh Testimonial'),
        'not_found' => __('No Testimonial Found')
    );

    $args = array(
        'supports' => $supports,
        'labels' => $labels,
        'public' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'news'),
        'has_archive' => true,
        'hierarchical' =>false,
        'menu_icon' => 'dashicons-heart',
        'menu_position' => 4
    );

    register_post_type('testimonial',$args);
}

add_action('init', 'testimonial_post_type');