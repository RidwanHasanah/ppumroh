<?php
class ThemePanel{

    public function __construct(){

        add_action('admin_menu', array( $this, 'theme_panel_menu' ));
    }

    public function theme_panel_menu(){
        add_menu_page(
            'Theme Option',
            'Theme Panel',
            'manage_options',//required
            'theme-options',//required
            array( $this, 'page_panel' ),
            'dashicons-admin-generic',
            2
        );

        add_submenu_page('theme-options', 'Slide', 'Slide Panel', 'manage_options','theme-panel-slide', array($this,'page_slide'));

        add_submenu_page('theme-options', 'Sosmed', 'Sosmed Panel', 'manage_options','theme-panel-sosmed', array($this,'page_sosmed'));

        add_submenu_page('theme-options', 'Video', 'Video Panel', 'manage_options','theme-panel-video', array($this,'page_video'));
    }

    public function page_panel(){
        if (isset($_POST['panel'])) {
            $options['package'] = $_POST['package'];
            $options['excellence'] = $_POST['excellence'];
            $options['testimonial'] = $_POST['testimonial'];
            $options['ads1'] = $_POST['ads1'];
            $options['ads2'] = $_POST['ads2'];
            $options['btm'] = $_POST['btm'];
            $options['wa'] = $_POST['wa'];

            update_option('panel',$options);
            echo '<div class="updated"><p><b>Option Saved </b></p></div>';

        }
        $options = get_option('panel');
        ?>
        <div class="wrap">
            <h1>Theme Panel</h1>
            <form action="" method="post" class="form">
                <table class="form-table">
                    <thead><h3>Title</h3></thead>
                    <tbody>
                        <tr>
                            <th>Paket</th>
                            <td>
                                <input value="<?php echo $options['package']; ?>" type="text" class="regular-text" id="package" name="package" placeholder="Paket Umroh">
                            </td>
                        </tr>
                        <tr>
                            <th>Keunggulan</th>
                            <td>
                                <input value="<?php echo $options['excellence']; ?>" type="text" class="regular-text" id="excellence" name="excellence" placeholder="Keunggulan Kami">
                            </td>
                        </tr>
                        <tr>
                            <th>Testimonial</th>
                            <td>
                                <input value="<?php echo $options['testimonial']; ?>" type="text" class="regular-text" id="testimonial" name="testimonial" placeholder="Testimonial">
                            </td>
                        </tr>
                        <tr>
                            <th>Bottom Footer</th>
                            <td>
                                  <textarea class="regular-text" name="btm" id="btm" rows="5"><?php echo $options['btm']; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>Nomor WhatsApp</th>
                            <td>
                                  <input placeholder="628571234567" type="text" class="regular-text" name="wa" id="wa" value="<?php echo $options['wa']; ?>">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <table class="form-table">
                    <thead><h3>Ads</h3></thead>
                    <tbody>
                        <tr>
                            <th>Ads 1</th>
                            <td>
                                  <textarea class="regular-text" name="ads1" id="ads1" rows="3"><?php echo $options['ads1']; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>Ads 2</th>
                            <td>
                                  <textarea class="regular-text" name="ads2" id="ads2" rows="3"><?php echo $options['ads2']; ?></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input type="submit" id="panel" name="panel" value="Save Changes" class="button-primary">

            </form>
        </div>
        <?php
    } 
    public function page_slide(){
        if(isset($_POST['amount_slide'])){
            $optionslides['slider'] = $_POST['slider'];

            update_option('amount_slide',$optionslides);
            echo '<div class="updated"><p><b>Option Saved </b></p></div>';
        }

        $optionslides = get_option('amount_slide');

        if ($optionslides['slider'] < 1){ $slide = 1; }
        else { $slide = $optionslides['slider']; }

        if(isset($_POST['slide'])){

            for ($i=1; $i <= $slide ; $i++) { 
                $options["stitle$i"] = $_POST["stitle$i"];
                $options["sdesc$i"] = $_POST["sdesc$i"];
                $options["sbtn$i"] = $_POST["sbtn$i"];
                $options["simg$i"] = $_POST["simg$i"];
            }

            update_option('slide', $options);
            echo '<div class="updated"><p><b>Option Saved </b></p></div>';

        }
        $options = get_option('slide');
        ?>
        <div class="wrap">
            <h1>Theme Panel Slide</h1>

            <form action="" method="post" class="form">
                <table class="form-table">
                    <tbody>
                        <tr>
                        <th>Total Slide</th>
                        <td><input value="<?php echo $optionslides["slider"]; ?>" class="regular-text" type="number" name="slider" id="slider"></td>
                        </tr>
                    </tbody>
                </table>
                <input type="submit" id="amount_slide" name="amount_slide" value="Save Changes" class="button-primary">
            </form>

            <hr style="border : 1px solid #000 ">

            <form action="" method="post" class="form">
                <?php
                for ($i= 1; $i <= $slide; $i++) { 
                   ?>
                    <table class="form-table">
                        <thead>
                            <h3>Slide <?php echo $i; ?></h3>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Title</th>
                                <td><input value="<?php echo $options["stitle$i"]; ?>" class="regular-text" type="text" name="stitle<?php echo $i; ?>" id="stitle<?php echo $i; ?>"></td>

                                <th>Description</th>
                                <td><input value="<?php echo $options["sdesc$i"]; ?>" class="regular-text" type="text" name="sdesc<?php echo $i; ?>" id="sdesc<?php echo $i; ?>"></td>
                            </tr>
                            <tr>
                                <th>Link Button</th>
                                <td><input value="<?php echo $options["sbtn$i"]; ?>" class="regular-text" type="link" name="sbtn<?php echo $i; ?>" id="sbtn<?php echo $i; ?>"></td>
                                
                                <th>Image Source Link <small>ukuran 1351x634</small></th>
                                <td><input value="<?php echo $options["simg$i"]; ?>" class="regular-text" type="link" name="simg<?php echo $i; ?>" id="simg<?php echo $i; ?>"></td>
                            </tr>
                        </tbody>
                    </table>
                    <hr style="border : 1px solid #000 ">
                   <?php
                }
                ?>
                <input type="submit" id="slide" name="slide" value="Save Changes" class="button-primary">
            </form>
        </div>
        <?php
    }

    public function page_sosmed(){
        if (isset($_POST['sosmed'])){

            $options['cfb'] = $_POST['cfb'];
            $options['ctwitter'] = $_POST['ctwitter'];
            $options['cgoogle'] = $_POST['cgoogle'];
            $options['cinsta'] = $_POST['cinsta'];

            $options['fb'] = $_POST['fb'];
            $options['twitter'] = $_POST['twitter'];
            $options['google'] = $_POST['google'];
            $options['insta'] = $_POST['insta'];

            update_option('sosmed',$options);
            echo '<div class="updated"><p><b>Option Saved </b></p></div>';
        }
        $options = get_option('sosmed');
        ?>
        <div class="wrap">
            <h1>Theme Panel Sosmed</h1>
            <form action="" method="post" class="form">
                <table class="form-table">
                    <tbody>
                        <tr>
                            <th>Facebook</th>
                            <td>
                                <label for="">
                                    <input <?php echo $options['cfb']==1?'checked':''; ?> type="checkbox" id="cfb" name="cfb" value="1"> Active Facebook Link
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th>Twitter</th>
                            <td>
                                <label for="">
                                    <input <?php echo $options['ctwitter']==1?'checked':''; ?> type="checkbox" id="ctwitter" name="ctwitter" value="1"> Active Twitter Link
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th>Google</th>
                            <td>
                                <label for="">
                                    <input <?php echo $options['cgoogle']==1?'checked':''; ?> type="checkbox" id="cgoogle" name="cgoogle" value="1"> Active Google Link
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th>Instagram</th>
                            <td>
                                <label for="">
                                    <input <?php echo $options['cinsta']==1?'checked':''; ?> type="checkbox" id="cinsta" name="cinsta" value="1"> Active Instagram Link
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="">Facebook Link</label></th>
                            <td><input class="regular-text" type="link" id="fb" name="fb" value="<?php echo $options['fb']; ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="">Twitter Link</label></th>
                            <td><input class="regular-text" type="link" id="twitter" name="twitter" value="<?php echo $options['twitter']; ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="">Google+ Link</label></th>
                            <td><input class="regular-text" type="link" id="google" name="google" value="<?php echo $options['google']; ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="">Instagram Link</label></th>
                            <td><input class="regular-text" type="link" id="insta" name="insta" value="<?php echo $options['insta']; ?>"></td>
                        </tr>
                    </tbody>
                    
                </table>
                <input type="submit" id="sosmed" name="sosmed" value="Save Changes" class="button-primary">
            </form>
        </div>
        <?php
    }

    public function page_video(){
        if (isset($_POST['video'])) {
            $options['v1'] = $_POST['v1'];
            $options['v2'] = $_POST['v2'];
            $options['v3'] = $_POST['v3'];
            $options['v4'] = $_POST['v4'];

            update_option('video',$options);
            echo '<div class="updated"><p><b>Option Saved </b></p></div>';

        }

        $options = get_option('video');
        ?>
        <div class="wrap">
            <h1>Video</h1>
            <p>Ambil id Video youtube contoh https://www.youtube.com/watch?v=<b>mM0b8YHbRGA</b></p>
            <form action="" method="post" class="form">
                <table class="form-table">
                    <tbody>
                        <tr>
                            <th>Video 1</th>
                            <td><input type="text" id="v1" name="v1" class="regular-text" value="<?php echo $options['v1']; ?>" placeholder="example : bXJ3ypd3_f8 " ></td>
                        </tr>
                        <tr>
                            <th>Video 2</th>
                            <td><input type="text" id="v2" name="v2" class="regular-text" value="<?php echo $options['v2']; ?>" placeholder="example : bXJ3ypd3_f8 " ></td>
                        </tr>
                        <tr>
                            <th>Video 3</th>
                            <td><input type="text" id="v3" name="v3" class="regular-text" value="<?php echo $options['v3']; ?>" placeholder="example : bXJ3ypd3_f8 " ></td>
                        </tr>
                        <tr>
                            <th>Video 4</th>
                            <td><input type="text" id="v4" name="v4" class="regular-text" value="<?php echo $options['v4']; ?>" placeholder="example : bXJ3ypd3_f8 " ></td>
                        </tr>
                    </tbody>
                </table>

                <input type="submit" id="video" name="video" value="Save Changes" class="button-primary">

            </form>
        </div>
        <?php
    }

    
}
if( is_admin() )
    $themepanel = new ThemePanel();


// Added Display Option On Dashboard
// require_once('theme_panel_sosmed.php');
?>