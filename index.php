<?php get_header(); ?>
<div class="container-fluid mt-5 pt-5 text-center">
<section>
    <?php
    if(is_page()) echo '<h1>'.get_the_title().'</h1>';
    else echo '<h1> Blog</h1>';
    ?>
<hr class="line-ember">
</section>
<div class="row p-2">
<div class="col-md-12">
<main>
  <div class="row p-2">
    <?php
    if( have_posts())
    {
    while(have_posts())
    {
        the_post();
        get_template_part('content',get_post_format());
    }   
    }else 
    {
        echo 'Tidak Ada Post';    
    }
    ?>
  </div>
</main>
</div>
<div class="col-md-3">
<!-- <aside>
    <?php //dynamic_sidebar('sidebar1');?>
    <?php //dynamic_sidebar('sidebar2');?>
</aside> -->
</div>
</div>
<?php mdb_pagination(); ?>
</div>
<!-- <div class="clear"></div> -->
<?php get_footer(); ?>
