<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php bloginfo('name'); ?></title>
  <?php wp_head(); ?>
  <link rel="shortcut icon" href="https://webicdn.com/sdirmember/29/28864/logotoko/rnkfc.png">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://webicdn.com/sdirmember/29/28864/logotoko/v5y5g.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://webicdn.com/sdirmember/29/28864/logotoko/v5y5g.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://webicdn.com/sdirmember/29/28864/logotoko/v5y5g.png">
  
  <link rel="stylesheet" href="">
</head>
<body>
<?php require_once('components/front-page/nav.inc.php');?>